var util = require('../../utils/util.js');
var api = require('../../config/api.js');

Page({

  switchCate: function(event) {
    wx.navigateTo({
      url: '../hotGoods/hotGoods?categoryId=' + event.currentTarget.dataset.id + 
      '&categoryNAME=' + event.currentTarget.dataset.name + 
      '&categoryPic=' + event.currentTarget.dataset.picurl
    })
  },

  showPopup() {
    this.setData({ show: true });
    },
    onClose() {
    this.setData({ show: false });
    },

 data:{
  goodsCount: 0,
  goodsList: [],
  categoryList: [],
  columns:[],
  categoryNAME: '所有商品',
  categoryPic: '',
  currentSortType: 'default',
  currentSort: 'add_time',
  currentSortOrder: 'desc',
  page: 1,
  limit: 10,
  categoryId: -1,
  show: false,
  scrollLeft: 0,
  scrollTop: 0,
  scrollHeight: 0,
  pages:1, //总页数
 },

 getGoodsList: function() {
  var that = this;
  util.request(api.GoodsList, {
      /*
      isHot: true,
      */
      page: that.data.page,
      limit: that.data.limit,
      order: that.data.currentSortOrder,
      sort: that.data.currentSort,
      categoryId: that.data.categoryId
    }).then(function(res) {
      if (res.errno === 0) {
        that.setData({
          goodsList: res.data.list,
          filterCategory: res.data.filterCategoryList
        });
      }
    });
},

getCatalog: function() {
  let that = this;
  util.request(api.CatalogList).then(function(res) {
    that.setData({
      categoryList: res.data.categoryList
    });
  });
},


onReady: function() {
  // 页面渲染完成
},
onShow: function() {
  // 页面显示
},
onHide: function() {
  // 页面隐藏
},
onUnload: function() {
  // 页面关闭
},


  onLoad: function(options) {

    this.getTabBar().setData({
      active : 1
    })

    /*
    util.request(api.GoodsCount).then(function(res) {
      that.setData({
        goodsCount: res.data
      });
    });
    */
     // 页面初始化 options为页面跳转所带来的参数
     this.getGoodsList();
    // 商品分类
    this.getCatalog();
  },
  onPullDownRefresh() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    this.getCatalog();
    wx.hideNavigationBarLoading() //完成停止加载
    wx.stopPullDownRefresh() //停止下拉刷新
  },


  openSortFilter: function(event) {
    let currentId = event.currentTarget.id;
    switch (currentId) {
      case 'categoryFilter':
        this.setData({
          categoryFilter: !this.data.categoryFilter,
          currentSortType: 'category',
          currentSort: 'add_time',
          currentSortOrder: 'desc'
        });
        break;
      case 'priceSort':
        let tmpSortOrder = 'asc';
        if (this.data.currentSortOrder == 'asc') {
          tmpSortOrder = 'desc';
        }
        this.setData({
          currentSortType: 'price',
          currentSort: 'retail_price',
          currentSortOrder: tmpSortOrder,
          categoryFilter: false
        });

        this.getGoodsList();
        break;
      default:
        //综合排序
        this.setData({
          currentSortType: 'default',
          currentSort: 'add_time',
          currentSortOrder: 'desc',
          categoryFilter: false,
          categoryId: 0,
        });
        this.getGoodsList();
    }
  },
  selectCategory: function(event) {
    let currentIndex = event.target.dataset.categoryIndex;
    this.setData({
      'categoryFilter': false,
      'categoryId': this.data.filterCategory[currentIndex].id
    });
    this.getGoodsList();
  }

    /*
  data: {
    categoryList: [],
    currentCategory: {},
    currentSubCategoryList: {},
    scrollLeft: 0,
    scrollTop: 0,
    goodsCount: 0,
    scrollHeight: 0
  },
  */
  
 
  /*
  getCurrentCategory: function(id) {
    let that = this;
    util.request(api.CatalogCurrent, {
        id: id
      })
      .then(function(res) {
        that.setData({
          currentCategory: res.data.currentCategory,
          currentSubCategoryList: res.data.currentSubCategory
        });
      });
  },
  */

  /*
  switchCate: function(event) {
    var that = this;
    var currentTarget = event.currentTarget;
    if (this.data.currentCategory.id == event.currentTarget.dataset.id) {
      return false;
    }

    this.getCurrentCategory(event.currentTarget.dataset.id);
  }
  */

})