Component({

  data: {
    active: null,
    backgroundColor: "#fafafa",
    borderStyle: "white",
    selectedColor: "#AB956D",
    color: "#666",
    list: [{
      pagePath: "/pages/index/index",
      normalIcon: "wap-home-o",
      activeIcon: "wap-home"
    },
    {
      pagePath: "/pages/catalog/catalog",
      normalIcon: "fire-o",
      activeIcon: "fire"
    },
    {
      pagePath: "/pages/cart/cart",
      normalIcon: "shopping-cart-o",
      activeIcon: "shopping-cart"
    },
    {
      pagePath: "/pages/ucenter/index/index",
      normalIcon: "manager-o",
      activeIcon: "manager"
    }]
  },
  
  attached() {
  },
  methods: {
    /*
    switchTab(e) {
      const data = e.currentTarget.dataset
      const url = data.path
      let i = data.index
      wx.switchTab({url})
      this.setData({
        selected: i
      })
      console.log(data)
    },
    */

    onChange:function(event){
      const index = event.detail;
      wx.switchTab({
        url: this.data.list[index].pagePath
      })
    }

  },



  /*
  onShow: function () {
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      this.getTabBar().setData({
        selected: 0
      })
    }
  }
  */


})